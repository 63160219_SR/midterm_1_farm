/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm1;

/**
 *
 * @author sairu
 */
public class TestFarm {
    public static void main(String[] args) {
    Farm cow = new Farm("cow",5,10);
    cow.print();
    Farm giraffe = new Farm("giraffe",2.5,15);
    giraffe.print();
    Farm alligator = new Farm("alligator",3,7);
    alligator.print();
    Farm zebra = new Farm("zebra",10,30);
    zebra.print();
    
    cow.feed();
    cow.print();
    cow.buy();
    cow.print();
    giraffe.buy();
    giraffe.print();
    giraffe.buy();
    giraffe.print();
    giraffe.feed();
    giraffe.print();
    alligator.buy();
    alligator.print();
    alligator.feed();
    alligator.print();
    alligator.feed();
    alligator.print();
    zebra.sell();
    zebra.print();
    }
}
