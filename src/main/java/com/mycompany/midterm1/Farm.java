/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm1;

/**
 *
 * @author sairu
 */
import java.util.Scanner;

public class Farm {
    private String type;
    private double food;
    private int amount;
    
    public Farm (String type,double food,int amount){
        this.type = type;
        this.food = food;
        this.amount = amount;
    }
    public void print(){
        System.out.println("Animal type: " + Farm.this.type);
        System.out.println(Farm.this.type +" Food: " + Farm.this.food);
        System.out.println(Farm.this.type +" amount: " + Farm.this.amount);
        System.out.println("");
    }
    public void feed(){
        Scanner wr = new Scanner(System.in);
        double Feed = wr.nextDouble();
        if(Feed>this.food){
            System.out.println("Not enough food, need to buy more");
            return;
        }
        this.food = this.food-Feed;
    }
    public void buy(){
        Scanner wr = new Scanner(System.in);
        double buy = wr.nextDouble();
        this.food = this.food+buy;
        
    }
    public void sell(){
        Scanner wr = new Scanner(System.in);
        int sell = wr.nextInt();
        if(sell>this.amount){
            System.out.println("Number seven is not enough");
            return;
        }
        this.amount = this.amount-sell;
    }
}

